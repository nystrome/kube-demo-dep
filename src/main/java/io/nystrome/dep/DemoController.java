package io.nystrome.dep;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@Autowired
	DiscoveryClient discoveryClient;

	@Autowired
	private ServiceProperties serviceProperties;

	@GetMapping("/message")
	public String message() {
		return serviceProperties.getMessage();
	}

	@GetMapping("/services")
	public List<String> services() {
		return this.discoveryClient.getServices();
	}

	@GetMapping("/")
	public String index() {
		return "this is the index page";
	}
}
